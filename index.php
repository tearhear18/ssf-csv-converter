<?php 
require_once("csvlibs.php");

use app\library\ssfcsvmigration as ssfread;

$ep6 = array_map('str_getcsv', file('old.csv'));
$ep7 = array_map('str_getcsv', file('latest.csv'));

$csvBuilder = new ssfread;

$csvBuilder->setCsvRef( $ep6 );
$csvBuilder->setCsvReference( $ep7 );



try {

	$csvBuilder->isCsvReadable();
	$csvBuilder->rebuild();
	$csvBuilder->buildNewCsv();
	//$csvBuilder->doAttachment();
	$csvBuilder->debug();



} catch (\Exception $e) {
	echo $e->getMessage();
	exit();
}



?>
<!DOCTYPE html>
<html>
<head>
	<title>SSF CSV MIGRATOR</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	
</head>
<body>
<div class="container">
	<div class="leftbox lfloat">
		<p>EP 6 CSV READ HEAD 1</p>
		<br>
		<?php
			echo "<pre>";
			print_r($ep6[1]);

			
			
			echo "</pre>";
		?>

	</div>
	<div class="leftbox lfloat">
		<p>EP 6 CSV READ HEAD 1</p>
		<br>
		<?php
			echo "<pre>";
			print_r($ep6[2][2]);
			print_r($ep6[3]);

			
			
			echo "</pre>";
		?>

	</div>
	<div class="leftbox lfloat">
		<p>NEW BUILD HEADER 1</p>
		<br>
		<?php
			echo "<pre>";
			print_r($csvBuilder->buildcsv[1]);
			
			echo "</pre>";
		?>

	</div>
	<div class="leftbox lfloat">
		<p>NEW BUILD HEADER VAL</p>
		<br>
		<?php
			echo "<pre>";
			print_r($csvBuilder->buildcsv[3]);
			
			echo "</pre>";
		?>

	</div>
	
	
</div>
</body>
</html>
