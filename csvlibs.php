<?php namespace app\library;
class ssfcsvmigration{

	var $oldcsv,
		$newcsv,
		$buildcsv;

	var $oldcsv_size,
	 	$newcsv_size;

	public function setCsvRef( $csv ){

		$this->oldcsv = $csv;
	}

	public function setCsvReference( $csv ){

		$this->newcsv = $csv;
	}

	public function isCsvReadable(){

		$nsize1 = sizeof($this->oldcsv);
		$nsize2 = sizeof($this->newcsv);

		if( ($nsize1 % 4) )	throw new \Exception("Unable to read old csv", 1);
		if( ($nsize2 % 4) )	throw new \Exception("Unable to read new csv", 1);

		$this->oldcsv_size = $nsize1;
		$this->newcsv_size = $nsize2;
	
	}

	public function rebuild(){
		$f = 0;
		$k = 1;
		$l = 2;
		$m = 3;
		for($a=0;$a<$this->oldcsv_size;$a++){
			if($f==$a):
				$this->buildcsv[$f]= $this->newcsv[0];
				$f+=4;
			
			elseif($k==$a):
				$this->buildcsv[$k]= $this->newcsv[1];
				$k+=4;
			
			elseif($l==$a):
				//build the 1st value
				$bf=[];
				foreach($this->newcsv[0] as $kj ){
					if(in_array($kj, $this->oldcsv[0])){
						$haystack = array_flip( $this->oldcsv[0]);
						//$key=$haystack[$k];
						$bf[]=$this->oldcsv[$l][$haystack[$kj]];
					}else{
						$bf[]=" ";
					}
					
				}
				$this->buildcsv[$l]= $bf;
				$l+=4;

			elseif($m==$a):
				$bf2=[];
				foreach($this->newcsv[1] as $kk ){
					if(in_array($kk, $this->oldcsv[1])){
						$haystack = array_flip( $this->oldcsv[1]);
						//$key=$haystack[$k];
						$bf2[]=$this->oldcsv[$m][$haystack[$kk]];
					}else{
						$bf2[]=" ";
					}
					
				}
				$this->buildcsv[$m]= $bf2;
				$m+=4;
			
			endif;

			
			
		}
		
		

	}
	public function buildNewCsv(){
		$fp = fopen('temp.csv', 'w');

		foreach ($this->buildcsv as $fields) {
		    fputcsv($fp, $fields);
		}

		fclose($fp);

	}
	public function doAttachment(){
		if (file_exists('temp.csv')) {

        //set appropriate headers
        header('Content-Description: File Transfer');
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename='.basename('new_build.csv'));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize('temp.csv'));
        ob_clean();
        flush();

        //read the file from disk and output the content.
        readfile('temp.csv');
        }
		
	}
	public function debug(){
		//print_r( $this->buildcsv);
		//print_r( $this->oldcsv);
		//print_r( $this->oldcsv_size);
		//print_r( $this->oldcsv_size );
	}


}